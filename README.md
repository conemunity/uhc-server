# Conemunity Server

Basic documentation about the setup for the Conemunity UHC server.

## Baremetal setup

Two system users have been added to the system:
```
sudo adduser \
    --system \
    --no-create-home \
    --group \
    minecraft

sudo adduser \
    --system \
    --home /opt/backups \
    --group \
    backups
```

The `minecraft` users is used to run the game. The backups user is used to create backups of the Minecraft server.

Minecraft runs in a docker container.
```
docker run -it -d \
    --name minecraft-cone \
    --hostname minecraft-cone \
    --restart unless-stopped \
    --volume /opt/minecraft/conemunity-uhc:/data \
    --env "TZ=America/New_York" \
    --publish 25565:25565 \
    --publish 127.0.0.1:9123:9123/tcp \
    --user 114:119 \
    --workdir /data \
    --log-opt max-size=1m --log-opt max-file=5 \
    eclipse-temurin:19.0.2_7-jdk java -XX:+UseZGC -Xmx11G -jar paper-1.20.1-115.jar nogui
```

The Minecraft data is located on the host at `/opt/minecraft/conemunity-uhc`.


## Paper configuration

paper-global.yml:
|Key|Value|Comment|
|---|-----|-------|
|/timings/enabled|false|Recommendation is to used the [Spark](https://spark.lucko.me/) plugin instead|

paper-world-defaults.yml
|Key|Value|Comment|
|---|-----|-------|
|/chunks/max-auto-save-chunks-per-tick|8|Reduces the amount of chunks saved per tick. At 20 ticks per second this still allows saving 160 chunks per second. Which is plenty but the change makes TPS more stable|
|/chunks/prevent-moving-into-unloaded-chunks|false|Moving into unloaded chunks causes a lot of problems in Minecraft and creates lag|
|/collisions/max-entity-collisions|2|Collision detection between many entities is costly|
|/entities/armor-stands/tick|false|Armor stands now longer flow in water. But helps with lag a bit|
|/entities/behavior/disable-chest-cat-detection|false|Chests no longer check if there is a cat on them. Reduces lag a bit, but also means that chests can be opened with a cat on it|
|/entities/spawning/despawn-ranges/ambient/hard|96|Reducing the range at which entities despawn. Thus fewer entities are loaded per player.|
|/entities/spawning/despawn-ranges/axolotls/hard|96|See above|
|/entities/spawning/despawn-ranges/creature/hard|96|See above|
|/entities/spawning/despawn-ranges/misc/hard|96|See above|
|/entities/spawning/despawn-ranges/monster/hard|96|See above|
|/entities/spawning/despawn-ranges/underground_water_creature/hard|96|See above|
|/entities/spawning/despawn-ranges/water_creature/hard|96|See above|
|/entities/spawning/non-player-arrow-despan-rate|'300'|Reducing the amount of ticks until a non-player arrow despawns. It is set to 15 seconds. Non-player arrows can't be picked up anyway. It reduces unnecessary entities in the world|
|/tick-rates/container-update|3|Container refresh their view every 3 ticks instead of every tick.|
|/tick-rates/grass-spread|4|Grass can spread only every 4 ticks instead of every tick.|
|/tick-rates/mob-spawner|2|Mob spawner can spawn only every 2 ticks instead of every tick.|

spigot.yml:
|Key|Value|Comment|
|---|-----|-------|
|/settings/restart-on-crash|false|Docker will do this for us|
|/world-settings/default/entity-activation-range/monsters|64|Monsters past 64 blocks won't move. This is an increase from the default of 32 on spigot, which is rather low. Environment deaths are important for UHC|
|/world-settings/default/entity-activation-range/flying-monsters|48|See above|
|/world-settings/default/entity-tracking-range/players|160|How far can you see other players in blocks. The default for spigot is really low|
|/world-settings/default/entity-tracking-range/animals|96|See above|
|/world-settings/default/entity-tracking-range/monsters|96|See above|
|/world-settings/default/entity-tracking-range/misc|80|See above|


## Backup

[Borg](https://www.borgbackup.org/) is used as the backup tool. In addition the borg repository is cloned to a remote server with rsync. Take a look at the `backup-minecraft.sh` script for more details.

The borg repository needs to be initialized first:
```
sudo -u backups borg init -e repokey /opt/backups/minecraft/server/conemunity-uhc-2023-08
```

In the `backup-minecraft.sh` scripts some variables need to be configured:

|Variable|Description|
|--------|-----------|
|`VAR_MC_SERVER_NAME`|The name of the docker container|
|`VAR_BACKUP_REMOTE_DIR`|The remote directory where the backup should be cloned to. This will be used by rsync as the destination|
|`VAR_BACKUP_REMOTE_SHELL`|The shell to be used for connection via rsync|
|`BORG_PASSPHRASE`|The passphrase set for the backups on the repository|

The crontab of the user backups is used.
