#!/usr/bin/env bash

VAR_MC_SERVER_NAME="minecraft-cone"
VAR_MC_BACKUP_SERVER_UP=`/usr/bin/docker inspect -f "{{ .State.Running }}" "$VAR_MC_SERVER_NAME"`

VAR_BACKUP_DIR="/opt/backups/minecraft/server/conemunity-uhc-2023-08"
VAR_BACKUP_SRC="/opt/minecraft/conemunity-uhc"
VAR_BACKUP_REMOTE_DIR="user@remote-location:basedir"
VAR_BACKUP_REMOTE_SHELL="ssh"

export BORG_BASE_DIR="/opt/backups"
export BORG_PASSPHRASE="****"

minecraft_server_command() {
  echo $2 | /usr/bin/socat EXEC:"/usr/bin/docker attach $1",pty STDIO
}

echo "==== $(date -Iseconds) ===="
echo "Starting backup"

if [ "$VAR_MC_BACKUP_SERVER_UP" == "true" ]; then
  echo "Setting save-off on Minecraft server"
  minecraft_server_command $VAR_MC_SERVER_NAME "say Starting backup..."
  minecraft_server_command $VAR_MC_SERVER_NAME "save-off"
  minecraft_server_command $VAR_MC_SERVER_NAME "save-all flush"
  sleep 5
else
  echo "Minecraft server not running"
fi

/usr/bin/borg create -v --stats --progress --compression lz4 \
        "$VAR_BACKUP_DIR::{hostname}-{now:%Y-%m-%dT%H-%M-%S}" \
        "$VAR_BACKUP_SRC"

if [ "$VAR_MC_BACKUP_SERVER_UP" == "true" ]; then
  echo "Setting save-on on Minecraft server"
  minecraft_server_command $VAR_MC_SERVER_NAME "say Finished backup"
  minecraft_server_command $VAR_MC_SERVER_NAME "save-on"
fi

/usr/bin/borg prune -v --stats -P '{hostname}-' --keep-within 24H -d 7 -w 4 "$VAR_BACKUP_DIR"
/usr/bin/borg compact --progress "$VAR_BACKUP_DIR"

rsync -rt --delete -e "$VAR_BACKUP_REMOTE_SHELL" "$VAR_BACKUP_DIR" "$VAR_BACKUP_REMOTE_DIR"

echo "Finished backup"
echo "==== $(date -Iseconds) ===="
echo
echo
